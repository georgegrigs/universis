import { Component, Input, OnInit, ViewChild, Output, EventEmitter, AfterViewInit } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { ActivatedRoute } from '@angular/router';
import { FormioComponent } from 'angular-formio/components/formio/formio.component';
import { TranslateService } from '@ngx-translate/core';
import { AdvancedFormsService } from './advanced-forms.service';
import {ErrorService} from '@universis/common';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'advanced-form-edit',
  // tslint:disable-next-line: max-line-length
  template: `<formio #form [form]="formConfig" (formLoad)='onLoad($event)' [submission]="{ data: data }" [refresh]="refreshForm"></formio>`,
  providers: [AdvancedFormsService]
})

export class AdvancedFormComponent implements OnInit {
  @Input('data') data: any;
  @Input('formName') formName: string;
  @ViewChild('form') form: FormioComponent;
  @Output() refreshForm = new EventEmitter<any>();
  formConfig: any = {};

  @Output() formSubmitted = new EventEmitter<any>();

  constructor(private _context: AngularDataContext,
    private _activatedRoute: ActivatedRoute,
    private _translateService: TranslateService,
    private _formService: AdvancedFormsService,
    private _errorService: ErrorService) {
  }

  onLoad(event) {
    const currentLang = this._translateService.currentLang;
    this.form.formio.i18next.options.resources[currentLang] = { translation : this._translateService.instant(`Forms`) };
    this.form.formio.language = currentLang;
  }

  async ngOnInit() {

    this._formService.loadForm(this.formName).then( result => {
      this.formConfig = result;
      this.refreshForm.emit(this.formConfig);
    });

    this.form.submitExecute = (submission: any) => {
      (async (submissionData: any) => {
        try {
          return await this._context.getService().execute({
            method: 'POST',
            url: this.formConfig.model,
            headers: {},
            data: submissionData.data
          });
        } catch (error) {
          throw error;
        }
      })(submission).then(() => {
        this.form.onSubmit(submission, true);
        //  Prepare message and notify advance-form-router about form submission
        let toastMessage;
        if ( this.formConfig.submitMessage && this.formConfig.submitMessage[this._translateService.currentLang] ) {
          toastMessage = {
            title: this.formConfig.submitMessage[this._translateService.currentLang].title,
            body: this.formConfig.submitMessage[this._translateService.currentLang].body
          };
        } else {
          toastMessage = {
            title: this._translateService.instant('Forms.FormIOdefaultSuccessfulSubmitTitle'),
            body: this._translateService.instant('Forms.FormIOdefaultSuccessfulSubmitMessage')
          };
        }
        const valueEmitted = { toastMessage };
        this.formSubmitted.emit(valueEmitted);
      }).catch( err => {
        if (err.status) {
          const translatedError = this._translateService.instant(`E${err.status}`);
          this._errorService.showError(translatedError.message,{
            continueLink: '.'
          });
          return this.form.onError(new Error(translatedError.message));
        }
        this._errorService.showError(err,{
          continueLink: '.'
        });
        this.form.onError(err);
      });
    };
  }
}
