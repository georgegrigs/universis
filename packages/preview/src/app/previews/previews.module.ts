import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {HighlightIncludeModule} from '../highlight/highlight.module';
import {PreviewsRoutingModule} from './previews.routing';
import {GraduationRulesComponent} from './components/graduation-rules/graduation-rules.component';

@NgModule({
  imports: [
    HighlightIncludeModule,
    PreviewsRoutingModule
  ],
  declarations: [
    GraduationRulesComponent,
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class PreviewsModule {}
