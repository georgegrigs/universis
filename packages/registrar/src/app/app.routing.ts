import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
// Layouts
import { FullLayoutComponent } from './layouts/full-layout.component';
import {AuthModule, AuthGuard} from '@universis/common';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {
    path: '',
    component: FullLayoutComponent,
    canActivate: [
      AuthGuard
    ],
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'dashboard',
        loadChildren: './dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'courses',
        loadChildren: './courses/courses.module#CoursesModule',
        data: {
          model: 'Courses'
        }
      },
      {
        path: 'classes',
        loadChildren: './classes/classes.module#ClassesModule',
        data: {
          model: 'CourseClasses',
          serviceParams: {'$expand': 'period,status,year,course($expand=department)'}
        }
      },
      {
        path: 'exams',
        loadChildren: './exams/exams.module#ExamsModule',
        data: {
          model: 'CourseExams',
          serviceParams: { '$expand': 'course,examPeriod,status,completedByUser,year' }
        }
      },
      {
        path: 'instructors',
        loadChildren: './instructors/instructors.module#InstructorsModule',
        data: {
          model: 'Instructors',
          serviceParams: { '$expand': 'user,status,country,workCountry,department($expand=organization)' }
        }
      },
      {
        path: 'students',
        loadChildren: './students/students.module#StudentsModule',
        data: {

          model: 'Students',
          serviceParams: { '$expand': 'person, studyProgram, user, department' }
        }
      },
      {
        path: 'study-programs',
        loadChildren: './study-programs/study-programs.module#StudyProgramsModule',
        data: {
          model: 'StudyPrograms',
          serviceParams: { '$expand': 'department, studyLevel, gradeScale' }
        }
      },
      {
        path: 'requests',
        loadChildren: './requests/requests.module#RequestsModule',
        data: {
          model: 'StudentRequests'
        }
      },
      {
        path: 'theses',
        loadChildren: './theses/theses.module#ThesesModule',
        data: {
          model: 'Theses',
          serviceParams: {'$expand': 'instructor,type,status'}
        }
      },
      {
        path: 'scholarships',
        loadChildren: './scholarships/scholarships.module#ScholarshipsModule',
        data: {
          model: 'Scholarships',
          serviceParams: { '$expand': 'department, organization' }
        }
      },
      {
        path: 'internships',
        loadChildren: './internships/internships.module#InternshipsModule',
        data: {
          model: 'Internships',
          serviceParams: { '$expand': 'status, internshipPeriod, department, student($expand=person, studentStatus, studyProgram, inscriptionYear, user)' }
        }
      },
      {
        path: 'registrations',
        loadChildren: './registrations/registrations.module#RegistrationsModule',
        data: {
          model: 'StudentPeriodRegistrations'
        }
      },
      {
        path: 'departments',
        loadChildren: './departments/departments.module#DepartmentsModule',
        data: {
          model: 'LocalDepartments'
        }
      },
      {
        path: 'users',
        loadChildren: './users/users.module#UsersModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
        paramsInheritanceStrategy: 'always'
      }), AuthModule],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
