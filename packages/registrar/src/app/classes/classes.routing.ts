import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ClassesHomeComponent} from './components/classes-home/classes-home.component';
import {ClassesTableComponent} from './components/classes-table/classes-table.component';
import {ClassesPreviewComponent} from './components/classes-preview/classes-preview.component';
import {ClassesRootComponent} from './components/classes-root/classes-root.component';
import {ClassesPreviewGeneralComponent} from './components/classes-preview-general/classes-preview-general.component';
import {ClassesPreviewStudentsComponent} from './components/classes-preview-students/classes-preview-students.component';
import {ClassesPreviewInstructorsComponent} from './components/classes-preview-instructors/classes-preview-instructors.component';
import { AdvancedFormRouterComponent } from '../registrar-shared/advanced-form-router/advanced-form-router.component';
import {AdvancedFormItemResolver, AdvancedFormModalComponent, AdvancedFormModalData} from '@universis/forms';
import {InstructorsSharedModule} from '../instructors/instructors.shared';
import {ClassesAddInstructorComponent} from "./components/classes-preview-instructors/classes-add-instructor.component";

const routes: Routes = [
    {
        path: '',
        component: ClassesHomeComponent,
        data: {
            title: 'Classes'
        },
        children: [
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'list'
            },
            {
                path: 'list',
                component: ClassesTableComponent,
                data: {
                    title: 'Classes List'
                }
            },
            {
                path: 'active',
                component: ClassesTableComponent,
                data: {
                    title: 'Active Classes'
                }
            }
        ]
    },
    {
      path: 'new',
      component: AdvancedFormRouterComponent
    },
    {
        path: ':id',
        component: ClassesRootComponent,
        data: {
            title: 'Classes Home'
        },
        children: [
          {
            path: '',
            pathMatch: 'full',
            redirectTo: 'preview'
          },
          {
            path: 'preview',
            component: ClassesPreviewComponent,
            data: {
                title: 'Classes Preview'
            },
            children: [
              {
                path: '',
                redirectTo: 'general'
              },
              {
                path: 'general',
                component: ClassesPreviewGeneralComponent,
                data: {
                  title: 'Classes Preview General'
                }
              },
              {
                path: 'students',
                component: ClassesPreviewStudentsComponent,
                data: {
                  title: 'Classes Preview Students'
                }
              },
              {
                path: 'instructors',
                component: ClassesPreviewInstructorsComponent,
                data: {
                  title: 'Classes Preview Instructors'
                },
                children: [
                  {
                    path: 'add',
                    pathMatch: 'full',
                    component: ClassesAddInstructorComponent,
                    outlet: 'modal',
                    data: <AdvancedFormModalData> {
                      title: 'Classes.AddInstructor',
                      config: InstructorsSharedModule.InstructorsList
                    },
                    resolve: {
                      courseClass: AdvancedFormItemResolver
                    }
                  }
                ]

              }
            ]
          },
          {
            path: ':action',
            component: AdvancedFormRouterComponent
          }
        ]
      }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: []
})
export class ClassesRoutingModule {
}
