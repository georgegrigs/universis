import {NgModule, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {environment} from '../../environments/environment';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {ClassesPreviewFormComponent} from './components/classes-preview-form/classes-preview-form.component';
import {SharedModule} from '@universis/common';
import {FormsModule} from '@angular/forms';
import * as DEFAULT_CLASSES_LIST from './components/classes-table/default-classes-table.config.json';
@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    SharedModule,
    FormsModule
  ],
  declarations: [
    ClassesPreviewFormComponent
  ],
  exports: [
    ClassesPreviewFormComponent
  ]
})
export class ClassesSharedModule implements OnInit {

  public static readonly DefaultClassList = DEFAULT_CLASSES_LIST;

  constructor(private _translateService: TranslateService) {
    this.ngOnInit().catch(err => {
      console.error('An error occurred while loading classes shared module');
      console.error(err);
    });
  }

  async ngOnInit() {
    environment.languages.forEach( language => {
      import(`./i18n/classes.${language}.json`).then((translations) => {
        this._translateService.setTranslation(language, translations, true);
      });
    });
  }

}
