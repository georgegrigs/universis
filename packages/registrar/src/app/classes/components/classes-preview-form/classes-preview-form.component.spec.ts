import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassesPreviewFormComponent } from './classes-preview-form.component';
import {APP_BASE_HREF, CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {MostModule} from '@themost/angular';
import {ConfigurationService} from '@universis/common';
import {TestingConfigurationService} from '../../../test';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {FormsModule} from '@angular/forms';

describe('ClassesPreviewFormComponent', () => {
  let component: ClassesPreviewFormComponent;
  let fixture: ComponentFixture<ClassesPreviewFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ClassesPreviewFormComponent
      ],
      imports: [
        CommonModule,
        FormsModule,
        RouterModule.forRoot([]),
        TranslateModule.forRoot(),
        MostModule.forRoot({
          base: '/',
          options: {
            useMediaTypeExtensions: false
          }
        })
      ],
      providers: [
        {
          provide: APP_BASE_HREF,
          useValue: '/'
        },
        {
          provide: ConfigurationService,
          useClass: TestingConfigurationService
        }
      ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    })
        .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassesPreviewFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
