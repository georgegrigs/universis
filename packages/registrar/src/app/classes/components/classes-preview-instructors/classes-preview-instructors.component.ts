import {Component, EventEmitter, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AdvancedTableComponent, AdvancedTableConfiguration, AdvancedTableDataResult} from '../../../tables/components/advanced-table/advanced-table.component';
import {AngularDataContext} from '@themost/angular';
import * as CLASSES_INSTRUCTORS_LIST_CONFIG from './classes-preview-instructors.config.json';
import {DIALOG_BUTTONS, ErrorService, ModalService, ToastService} from '@universis/common';
import {TranslateService} from '@ngx-translate/core';
import {Subscription} from 'rxjs';
import {ActivatedTableService} from "../../../tables/tables.activated-table.service";


@Component({
  selector: 'app-classes-preview-instructors',
  templateUrl: './classes-preview-instructors.component.html'
})
export class ClassesPreviewInstructorsComponent implements OnInit, OnDestroy {

  public readonly config: AdvancedTableConfiguration = <AdvancedTableConfiguration> CLASSES_INSTRUCTORS_LIST_CONFIG;
  @ViewChild('instructors') instructors: AdvancedTableComponent;
  courseClassID: any = this._activatedRoute.snapshot.params.id;
  public recordsTotal: any;
  private fragmentSubscription: Subscription;
  @Input() reload: EventEmitter<any> = new EventEmitter<any>();


  constructor(private _activatedRoute: ActivatedRoute,
              private _translateService: TranslateService,
              private _activatedTable: ActivatedTableService,
              private _errorService: ErrorService,
              private _modalService: ModalService,
              private _toastService: ToastService,
              private _context: AngularDataContext

  ) { }

  async ngOnInit() {
    this._activatedTable.activeTable = this.instructors;

    this.instructors.query =  this._context.model('CourseClasses/' + this._activatedRoute.snapshot.params.id + '/instructors')
      .asQueryable()
      .expand('instructor')
      .prepare();

    this.instructors.config = AdvancedTableConfiguration.cast(CLASSES_INSTRUCTORS_LIST_CONFIG);
    this.fragmentSubscription = this._activatedRoute.fragment.subscribe( fragment => {
      if (fragment && fragment === 'reload') {
        this.instructors.fetch(true);
      }
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;

  }

  ngOnDestroy(): void {
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
  }
  remove() {
    if (this.instructors && this.instructors.selected && this.instructors.selected.length) {
      const items = this.instructors.selected.map( item => {
        return {
          courseClass: this.courseClassID,
          instructor: item.id
        };
      });
      return this._modalService.showWarningDialog(
        this._translateService.instant('Classes.RemoveInstructorTitle'),
        this._translateService.instant('Classes.RemoveInstructorMessage'),
        DIALOG_BUTTONS.OkCancel).then( result => {
        if (result === 'ok') {
          this._context.model('courseClassInstructors').remove(items).then( () => {
            this._toastService.show(
              this._translateService.instant('Classes.RemoveInstructorsMessage.title'),
              this._translateService.instant((items.length === 1 ?
                'Classes.RemoveInstructorsMessage.one' : 'Classes.RemoveInstructorsMessage.many')
                , { value: items.length })
            );
            this.instructors.fetch(true);
          }).catch( err => {
            this._errorService.showError(err, {
              continueLink: '.'
            });
          });
        }
      });

    }
  }


}
