import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassesPreviewStudentsComponent } from './classes-preview-students.component';

describe('ClassesPreviewStudentsComponent', () => {
  let component: ClassesPreviewStudentsComponent;
  let fixture: ComponentFixture<ClassesPreviewStudentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassesPreviewStudentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassesPreviewStudentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
