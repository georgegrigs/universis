import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';

@Component({
  selector: 'app-classes-root',
  templateUrl: './classes-root.component.html',
})
export class ClassesRootComponent implements OnInit {
  public model: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) { }

  async ngOnInit() {
    this.model = await this._context.model('CourseClasses')
      .where('id').equal(this._activatedRoute.snapshot.params.id)
      .expand('course,status,period')
      .getItem();
  }

}
