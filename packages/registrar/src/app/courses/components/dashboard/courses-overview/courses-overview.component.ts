import { Component, OnInit, Input } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';

@Component({
  selector: 'app-courses-overview',
  templateUrl: './courses-overview.component.html',
  styleUrls: ['./courses-overview.component.scss']
})
export class CoursesOverviewComponent implements OnInit {
  public currentYear: any;
  constructor(public _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) { }

  async ngOnInit() {

    this.currentYear = await this._context.model('Courses')
      .where('id').equal(this._activatedRoute.snapshot.params.id)
      .expand ('department($expand=currentYear)')
      .select('department/currentYear/alternateName', 'name')
      .getItem();
  }

}
