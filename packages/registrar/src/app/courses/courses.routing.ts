import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CoursesHomeComponent} from './components/courses-home/courses-home.component';
import {CoursesTableComponent} from './components/courses-table/courses-table.component';
import {CoursesRootComponent} from './components/courses-root/courses-root.component';
import {CoursesPreviewGeneralComponent} from './components/dashboard/courses-preview-general/courses-preview-general.component';
import {CoursesPreviewClassesComponent} from './components/dashboard/courses-preview-classes/courses-preview-classes.component';
import {CoursesOverviewComponent} from './components/dashboard/courses-overview/courses-overview.component';
import {CoursesExamsComponent} from './components/dashboard/courses-exams/courses-exams.component';
import {CoursesStudyProgramsComponent} from './components/dashboard/courses-study-programs/courses-study-programs.component';
import {AdvancedFormRouterComponent} from './../registrar-shared/advanced-form-router/advanced-form-router.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';

const routes: Routes = [
    {
        path: '',
        component: CoursesHomeComponent,
        data: {
            title: 'Courses'
        },
        children: [
          {
            path: '',
            pathMatch: 'full',
            redirectTo: 'list'
          },
          {
            path: 'list',
            component: CoursesTableComponent,
            data: {
              title: 'Courses List'
            }
          },
          {
            path: 'active',
            component: CoursesTableComponent,
            data: {
              title: 'Active Courses'
            }
          }
        ]
    }, {
      path: 'new',
      component: AdvancedFormRouterComponent,
    },
    {
      path: ':id',
      component: CoursesRootComponent,
      data: {
        title: 'Courses Root'
      },
      children: [
            {
              path: '',
              pathMatch: 'full',
              redirectTo: 'dashboard'
            },
            {
              path: 'dashboard',
              component: DashboardComponent,
              data: {
                title: 'Courses.Overview'
              },
              children: [
                {
                  path: '',
                  redirectTo: 'overview',
                  pathMatch: 'full'
                },
                {
                  path: 'overview',
                  component: CoursesOverviewComponent,
                  data: {
                    title: 'Courses.Overview'
                  }
                },
                {
                  path: 'general',
                  component: CoursesPreviewGeneralComponent,
                  data: {
                    title: 'Courses.General'
                  }
                },
                {
                  path: 'classes',
                  component: CoursesPreviewClassesComponent,
                  data: {
                    title: 'Courses.Classes'
                  }
                },
                {
                  path: 'exams',
                  component: CoursesExamsComponent,
                  data: {
                    title: 'Courses.Exams'
                  }
                },
                {
                  path: 'study-programs',
                  component: CoursesStudyProgramsComponent,
                  data: {
                    title: 'Courses.StudyPrograms'
                  }
                }
              ]
            },
            {
              path: ':action',
              component: AdvancedFormRouterComponent,
              data: {
                title: 'Courses.Edit'
              }
            }
          ]
        }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: []
})
export class CoursesRoutingModule {
}
