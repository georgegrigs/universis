import {CUSTOM_ELEMENTS_SCHEMA, NgModule, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {environment} from '../../environments/environment';
import {TranslateModule, TranslateService} from '@ngx-translate/core';

import { DashboardDepartmentInfoComponent } from './components/dashboard-department-info/dashboard-department-info.component';
import { DashboardQuickSearchComponent } from './components/dashboard-quick-search/dashboard-quick-search.component';
import { DashboardQuickActionsComponent } from './components/dashboard-quick-actions/dashboard-quick-actions.component';
import { DashboardPendingStudentRequestsComponent } from './components/dashboard-pending-student-requests/dashboard-pending-student-requests.component';
import { DashboardExamDocumentsComponent } from './components/dashboard-exam-documents/dashboard-exam-documents.component';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule


  ],
  declarations: [
    DashboardDepartmentInfoComponent, DashboardQuickSearchComponent,
    DashboardQuickActionsComponent, DashboardPendingStudentRequestsComponent,
    DashboardExamDocumentsComponent
  ],
  exports: [
    DashboardDepartmentInfoComponent, DashboardQuickSearchComponent,
    DashboardQuickActionsComponent, DashboardPendingStudentRequestsComponent,
    DashboardExamDocumentsComponent
  ],
  providers: [
    DashboardDepartmentInfoComponent, DashboardQuickSearchComponent,
    DashboardQuickActionsComponent, DashboardPendingStudentRequestsComponent,
    DashboardExamDocumentsComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]

})

export class DashboardSharedModule implements OnInit {
  constructor(private _translateService: TranslateService) {
    this.ngOnInit().catch(err => {
      console.error('An error occurred while loading dashboard module');
      console.error(err);
    });
  }

  async ngOnInit() {
    // create promises chain
    const sources = environment.languages.map(async (language) => {
      const translations = await import(`./i18n/dashboard.${language}.json`);
      this._translateService.setTranslation(language, translations, true);
    });
    // execute chain
    await Promise.all(sources);
  }
}
