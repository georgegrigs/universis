import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';

@Component({
  selector: 'app-exams-preview',
  templateUrl: './exams-preview.component.html',
  styleUrls: ['./exams-preview.component.scss']
})
export class ExamsPreviewComponent implements OnInit {

  public model: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext)  { }

  async ngOnInit() {
    this.model = await this._context.model('CourseExams')
    .where('id').equal(this._activatedRoute.snapshot.params.id)
    .select('id', 'name')
    .getItem();
  }

}
