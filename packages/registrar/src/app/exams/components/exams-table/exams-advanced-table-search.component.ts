import { Component, Input, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { AdvancedTableSearchComponent } from '../../../tables/components/advanced-table/advanced-table-search.component';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { DatePipe } from '@angular/common';
import { AdvancedTableSearchBaseComponent } from '../../../tables/components/advanced-table/advanced-table-search-base';

@Component({
    selector: 'app-exams-advanced-table-search',
    templateUrl: './exams-advanced-table-search.component.html',
    encapsulation: ViewEncapsulation.None
})

export class ExamsAdvancedTableSearchComponent extends AdvancedTableSearchBaseComponent {

    constructor(_context: AngularDataContext, _activatedRoute: ActivatedRoute, datePipe: DatePipe) {
        super();
    }
}
