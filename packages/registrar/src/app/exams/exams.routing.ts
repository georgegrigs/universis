import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ExamsHomeComponent } from './components/exams-home/exams-home.component';
import { ExamsTableComponent } from './components/exams-table/exams-table.component';
import { ExamsPreviewComponent } from './components/exams-preview/exams-preview.component';
import { ExamsRootComponent } from './components/exams-root/exams-root.component';
import { ExamsPreviewGeneralComponent } from './components/exams-preview-general/exams-preview-general.component';
import { ExamsPreviewStudentsComponent } from './components/exams-preview-students/exams-preview-students.component';
import { ExamsPreviewInstructorsComponent } from './components/exams-preview-instructors/exams-preview-instructors.component';
import { ExamsPreviewGradingComponent } from './components/exams-preview-grading/exams-preview-grading.component';
import { ExamsPreviewGradesCheckComponent } from './components/exams-preview-grades-check/exams-preview-grades-check.component';
import { AdvancedFormRouterComponent } from '../registrar-shared/advanced-form-router/advanced-form-router.component';
import {AdvancedFormItemResolver, AdvancedFormModalComponent, AdvancedFormModalData} from '@universis/forms';
import {InstructorsSharedModule} from '../instructors/instructors.shared';
import {ExamsAddInstructorComponent} from './components/exams-preview-instructors/exams-add-instructor.component';

const routes: Routes = [
  {
    path: '',
    component: ExamsHomeComponent,
    data: {
      title: 'Exams'
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list'
      },
      {
        path: 'list',
        component: ExamsTableComponent,
        data: {
          title: 'Exams List'
        }
      }
    ]
  },
  {
    path: ':id',
    component: ExamsRootComponent,
    data: {
      title: 'Exam Home'
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'preview'
      },
      {
        path: 'preview',
        component: ExamsPreviewComponent,
        data: {
          title: 'Exam Preview'
        },
        children: [
          {
            path: '',
            redirectTo: 'general'
          },
          {
            path: 'general',
            component: ExamsPreviewGeneralComponent,
            data: {
              title: 'Exams Preview General'
            }
          },
          {
            path: 'students',
            component: ExamsPreviewStudentsComponent,
            data: {
              title: 'Exams Preview Students'
            }
          },
          {
            path: 'instructors',
            component: ExamsPreviewInstructorsComponent,
            data: {
              title: 'Exams Preview Instructors'
            },
            children: [
              {
                path: 'add',
                pathMatch: 'full',
                component: ExamsAddInstructorComponent,
                outlet: 'modal',
                data: <AdvancedFormModalData> {
                  title: 'Exams.AddInstructor',
                  config: InstructorsSharedModule.InstructorsList
                },
                resolve: {
                  courseExam: AdvancedFormItemResolver
                }
              }
            ]
          },
          {
            path: 'grading',
            component: ExamsPreviewGradingComponent,
            data: {
              title: 'Exams Preview Grading'
            }
          },
          {
            path: 'grading/:id',
            component: ExamsPreviewGradesCheckComponent,
            data: {
              title: 'Exams Preview Display Grades'
            }
          }
        ]
      },
      {
        path: ':action',
        component: AdvancedFormRouterComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: []
})
export class ExamsRoutingModule {
}
