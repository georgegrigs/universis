import { Component, Input, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { DatePipe } from '@angular/common';
import { AdvancedTableSearchBaseComponent } from 'packages/registrar/src/app/tables/components/advanced-table/advanced-table-search-base';

@Component({
    selector: 'app-instructors-exams-advanced-table-search',
    templateUrl: './instructors-exams-advanced-table-search.component.html',
    encapsulation: ViewEncapsulation.None
})

export class InstructorsExamsAdvancedTableSearchComponent extends AdvancedTableSearchBaseComponent {

    constructor(_context: AngularDataContext, _activatedRoute: ActivatedRoute, datePipe: DatePipe) {
        super();
    }
}
