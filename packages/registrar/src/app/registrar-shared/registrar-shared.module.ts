import {CUSTOM_ELEMENTS_SCHEMA, ModuleWithProviders, NgModule, OnInit, Optional, SkipSelf} from '@angular/core';
import { CommonModule } from '@angular/common';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {environment} from '../../environments/environment';
import {ApplicationSettingsConfiguration, ErrorModule, SIDEBAR_LOCATIONS, AppSidebarService} from '@universis/common';
import {ActiveDepartmentService} from './services/activeDepartmentService.service';
import { AdvancedFormContainerComponent } from './advanced-form-container/advanced-form-container.component';
import {AdvancedFormsModule} from '@universis/forms';
import { AdvancedFormRouterComponent } from './advanced-form-router/advanced-form-router.component';
import {RouterModule} from '@angular/router';
import {MostModule} from '@themost/angular';
import {FormioModule} from 'angular-formio';
import * as sidebarLocations from '../app.sidebar.locations';


export declare interface ApplicationSettings extends ApplicationSettingsConfiguration {
  useDigitalSignature: boolean;
  title?: string;
}

@NgModule({
  imports: [
      CommonModule,
      RouterModule,
      TranslateModule,
      AdvancedFormsModule,
      ErrorModule,
      MostModule,
      FormioModule
  ],
  declarations: [
    AdvancedFormContainerComponent,
    AdvancedFormRouterComponent
  ],
  providers: [
    {
      provide: SIDEBAR_LOCATIONS,
      useValue: sidebarLocations.REGISTRAR_SIDEBAR_LOCATIONS
    }
  ],
  exports: [
    AdvancedFormContainerComponent,
    AdvancedFormRouterComponent
  ],
  schemas: [
      CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class RegistrarSharedModule implements OnInit {
  constructor(@Optional() @SkipSelf() parentModule: RegistrarSharedModule,
              private _translateService: TranslateService,
              private _sidebarService: AppSidebarService) {
    this.ngOnInit().catch(err => {
      console.error('An error occurred while loading registrar shared module');
      console.error(err);
    });
  }

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: RegistrarSharedModule,
      providers: [
        ActiveDepartmentService
      ]
    };
  }

  async ngOnInit() {
    // create promises chain
    const sources = environment.languages.map(async (language) => {
      const translations = await import(`../../assets/i18n/${language}.json`);
      this._translateService.setTranslation(language, translations, true);
    });
    // execute chain
    await Promise.all(sources);

    this._translateService.onDefaultLangChange.subscribe(() => {
      this._sidebarService.loadConfig();
    });
  }

}
