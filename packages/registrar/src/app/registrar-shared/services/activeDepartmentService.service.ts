import {UserStorageService} from '@universis/common';
import {EventEmitter, Injectable, Output} from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {AngularDataContext} from '@themost/angular';

@Injectable({
  providedIn: 'root'
})
export class ActiveDepartmentService {
  // Observable navItem source
  private _activeDepartmentSource = new BehaviorSubject<any>(0);
  // Observable navItem stream
  public departmentChange = this._activeDepartmentSource.asObservable();

  private activeDepartment: any;

  constructor( private _userStorage: UserStorageService) {
  }

  // get the active department
  async getActiveDepartment() {
    if (this.activeDepartment) {
      return this.activeDepartment.value;
    }
    const storageKey = 'registrar/departments/active';
    this.activeDepartment = await this._userStorage.getItem(storageKey);
    this._activeDepartmentSource.next(this.activeDepartment.value);
    return this.activeDepartment.value;
  }

  setActiveDepartment( activeDepartment: any ) {
    const storageKey = 'registrar/departments/active';
    this._userStorage.setItem(storageKey, JSON.parse(JSON.stringify(activeDepartment)));
    this.activeDepartment = { value: activeDepartment };
    this._activeDepartmentSource.next(activeDepartment);
  }
}

@Injectable({ providedIn: 'root' })
export class ActiveDepartmentResolver implements Resolve<any> {
  constructor(private _activeDepartmentService: ActiveDepartmentService) {}

  resolve(
      route: ActivatedRouteSnapshot,
      state: RouterStateSnapshot
  ): Promise<any>|any {
    return this._activeDepartmentService.getActiveDepartment();
  }
}

@Injectable({ providedIn: 'root' })
export class CurrentAcademicYearResolver implements Resolve<any> {
  constructor(private _activeDepartmentService: ActiveDepartmentService) {}

  resolve(
      route: ActivatedRouteSnapshot,
      state: RouterStateSnapshot
  ): Promise<any>|any {
    return this._activeDepartmentService.getActiveDepartment().then( res => {
       if (res) {
         return Promise.resolve(res.currentYear);
       }
       return Promise.resolve(null);
    });
  }
}

@Injectable({ providedIn: 'root' })
export class CurrentAcademicPeriodResolver implements Resolve<any> {
  constructor(private _activeDepartmentService: ActiveDepartmentService) {}

  resolve(
      route: ActivatedRouteSnapshot,
      state: RouterStateSnapshot
  ): Promise<any>|any {
    return this._activeDepartmentService.getActiveDepartment().then( res => {
      if (res) {
        return Promise.resolve(res.currentPeriod);
      }
      return Promise.resolve(null);
    });
  }
}

@Injectable({ providedIn: 'root' })
export class LastStudyProgramResolver implements Resolve<any> {
  constructor(private _context: AngularDataContext,
              private _activeDepartmentService: ActiveDepartmentService) {}

  resolve(
      route: ActivatedRouteSnapshot,
      state: RouterStateSnapshot
  ): Promise<any>|any {
    // get active department
    return this._activeDepartmentService.getActiveDepartment().then( res => {
      if (res) {
        // search department's study programs
        // get last item (make a trick here and get last program identifier because there is no other way to sort study programs)
        return this._context.model('StudyPrograms')
            .where('department').equal(res.id)
            .and('isActive').equal(true)
            .orderByDescending('id').getItem();
      }
      return Promise.resolve(null);
    });
  }
}
