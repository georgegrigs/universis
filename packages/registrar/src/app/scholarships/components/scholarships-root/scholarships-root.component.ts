import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';

@Component({
  selector: 'app-scholarships-root',
  templateUrl: './scholarships-root.component.html',

})
export class ScholarshipsRootComponent implements OnInit {
  public model: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) {
  }

  async ngOnInit() {
    this.model = await this._context.model('Scholarships')
      .where('id').equal(this._activatedRoute.snapshot.params.id)
      .expand('department')
      .getItem();
  }
}
