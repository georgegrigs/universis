import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ScholarshipsHomeComponent} from './components/scholarships-home/scholarships-home.component';
import {ScholarshipsTableComponent} from './components/scholarships-table/scholarships-table.component';
import {ScholarshipsPreviewComponent} from './components/scholarships-preview/scholarships-preview.component';
import {ScholarshipsRootComponent} from './components/scholarships-root/scholarships-root.component';
import {ScholarshipsPreviewGeneralComponent} from './components/scholarships-preview/scholarships-preview-general/scholarships-preview-general.component';
import {ScholarshipsPreviewStudentsComponent} from './components/scholarships-preview/scholarships-preview-students/scholarships-preview-students.component';
import {AdvancedFormRouterComponent} from '../registrar-shared/advanced-form-router/advanced-form-router.component';


const routes: Routes = [
    {
        path: '',
        component: ScholarshipsHomeComponent,
        data: {
            title: 'Scholarships'
        },
        children: [
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'list'
            },
            {
                path: 'list',
                component: ScholarshipsTableComponent,
                data: {
                    title: 'Scholarships List'
                }
            },
            {
                path: 'active',
                component: ScholarshipsTableComponent,
                data: {
                    title: 'Active Scholarships'
                }
            },
            {
              path: 'new',
              component: AdvancedFormRouterComponent
            }
        ]
    },
    {
        path: ':id',
        component: ScholarshipsRootComponent,
        data: {
            title: 'Scholarships Home'
        },
        children: [
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'preview'
            },
            {
                path: 'preview',
                component: ScholarshipsPreviewComponent,
                data: {
                    title: 'Scholarships Preview'
                },
                children: [
                    {
                        path: '',
                        redirectTo: 'general'
                    },
                    {
                        path: 'general',
                        component: ScholarshipsPreviewGeneralComponent,
                        data: {
                            title: 'Scholarships Preview General'
                        }
                    },
                    {
                        path: 'students',
                        component: ScholarshipsPreviewStudentsComponent,
                        data: {
                            title: 'Scholarships Preview Students'
                        }
                    }
                ]

            },
            {
              path: ':action',
              component: AdvancedFormRouterComponent
            }

        ]
  }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: []
})
export class ScholarshipsRoutingModule {
}
