import {Component, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute} from '@angular/router';
import {LoadingService} from '@universis/common';

@Component({
  selector: 'app-students-messages',
  templateUrl: './students-messages.component.html',
  styleUrls: ['./students-messages.component.scss']
})

export class StudentsMessagesComponent implements OnInit {
  
  public studentMessages: any;

  constructor(private _context: AngularDataContext,  private _activatedRoute: ActivatedRoute, private _loadingService: LoadingService) { }

  async ngOnInit() {
    this._loadingService.showLoading();
    this.studentMessages = await this._context.model('Students/' + this._activatedRoute.snapshot.params.id + '/messages')
      .asQueryable()
      .expand('action, sender','attachments')
      .orderByDescending('dateCreated')
      .take(-1)
      .getItems();
      this._loadingService.hideLoading();
  }
  
}
