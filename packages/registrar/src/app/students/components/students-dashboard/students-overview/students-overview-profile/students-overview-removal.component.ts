import { Component, OnInit, Input } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';

@Component({
  selector: 'app-students-overview-removal',
  templateUrl: './students-overview-removal.component.html'
})
export class StudentsOverviewRemovalComponent implements OnInit {

  public student;
  @Input() studentId: number;

  constructor(private _activatedRoute: ActivatedRoute, private _context: AngularDataContext) { }

  async ngOnInit() {
    this.student = await this._context.model('Students')
      .where('id').equal(this.studentId)
      .expand('person($expand=gender), department, studyProgram')
      .getItem();

  }

}
