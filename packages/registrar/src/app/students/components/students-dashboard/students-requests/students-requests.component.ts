import {Component, ElementRef, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {AngularDataContext} from '@themost/angular';
import {LoadingService} from '@universis/common';

@Component({
  selector: 'app-students-requests',
  templateUrl: './students-requests.component.html',
  styleUrls: ['./students-requests.component.scss']
})
export class StudentsRequestsComponent implements OnInit {
  public model: any;
  constructor(private _element: ElementRef,
              private _activatedRoute: ActivatedRoute,
              private _translate: TranslateService,
              private _loadingService: LoadingService,
              private _context: AngularDataContext) { }

  async ngOnInit() {
    this._loadingService.showLoading();
      this.model = await this._context.model('StudentRequestActions')
      .where('student').equal(this._activatedRoute.snapshot.params.id)
      .orderByDescending('dateCreated')
      .take(-1)
      .getItems();
    this._loadingService.hideLoading();
  }
}
