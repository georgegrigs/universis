import {Component, ElementRef, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {AngularDataContext} from '@themost/angular';

@Component({
  selector: 'app-students-theses',
  templateUrl: './students-theses.component.html'
})
export class StudentsThesesComponent implements OnInit {
  public model: any;

  constructor(private _element: ElementRef,
              private _activatedRoute: ActivatedRoute,
              private _translate: TranslateService,
              private _context: AngularDataContext) {
  }

  async ngOnInit() {
    this.model = await this._context.model('StudentTheses')
    .where('student').equal(this._activatedRoute.snapshot.params.id)
    .expand('thesis($expand=startYear,instructor,status,type)')
    .getItems();
  }
}
