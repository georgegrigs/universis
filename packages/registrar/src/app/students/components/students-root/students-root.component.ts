import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';

@Component({
  selector: 'app-students-root',
  templateUrl: './students-root.component.html',
  styleUrls: ['./students-root.component.scss']
})
export class StudentsRootComponent implements OnInit {
  public student: any;
  public tabs: any[];

   constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) {
  }

  async ngOnInit() {
     this.tabs = this._activatedRoute.routeConfig.children.filter( route => typeof route.redirectTo === 'undefined' );

    this.student = await this._context.model('Students')
      .where('id').equal(this._activatedRoute.snapshot.params.id)
      .expand('person, department, studyProgram')
      .getItem();
  }

}
