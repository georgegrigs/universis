import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs';
import {AdvancedTableComponent} from '../../../tables/components/advanced-table/advanced-table.component';
import {AdvancedSearchFormComponent} from '../../../tables/components/advanced-search-form/advanced-search-form.component';

@Component({
  selector: 'app-students-table',
  templateUrl: './students-table.component.html',
  styles: []
})
export class StudentsTableComponent implements OnInit, OnDestroy {

  private dataSubscription: Subscription;
  private paramSubscription: Subscription;
  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;
  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;

  constructor(private _activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.dataSubscription = this._activatedRoute.data.subscribe( data => {
      if (data.tableConfiguration) {
        this.table.config = data.tableConfiguration;
        this.table.query = null;
        this.table.ngOnInit();
      }
      if (data.searchConfiguration) {
        this.search.form = data.searchConfiguration;
        this.search.ngOnInit();
      }
    });
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }

}
