import { Component, OnInit } from '@angular/core';
import * as STUDYPROGRAMS_LIST_CONFIG from './study-programs-table.config.json';

@Component({
  selector: 'app-study-programs-table',
  templateUrl: './study-programs-table.component.html',
  styles: []
})
export class StudyProgramsTableComponent implements OnInit {

  public readonly config = STUDYPROGRAMS_LIST_CONFIG;

  constructor() { }

  ngOnInit() {
  }

}
