import {AfterViewInit, Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild, ViewEncapsulation} from '@angular/core';
import {FormioComponent, FormioForm} from 'angular-formio';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute} from '@angular/router';
import {ServiceUrlPreProcessor, TranslatePreProcessor} from '@universis/forms';
import {AdvancedTableSearchBaseComponent} from '../advanced-table/advanced-table-search-base';
import {TranslateService} from '@ngx-translate/core';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-advanced-search-form',
  template: `
  <div class="p-4">
    <formio #formioComponent (keydown)="onKeyEnter($event)" (change)="onChange($event)" (formLoad)="onFormLoad($event)"
            [refresh]="refreshForm" [submission]="submission" [form]="form"></formio>
  </div>
  `,
  styleUrls: ['./advanced-search-form.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AdvancedSearchFormComponent extends AdvancedTableSearchBaseComponent implements OnInit, OnDestroy, AfterViewInit {

  @ViewChild('formioComponent') formComponent: FormioComponent;
  @Input() form: any = {};
  @Output() refreshForm = new EventEmitter<any>();
  submission: any = { data: {} };
  private filterSubscription: Subscription;
  public loading = true;

  constructor(private _context: AngularDataContext,
              private _translateService: TranslateService,
              private _activatedRoute: ActivatedRoute) {
    super();
  }

  ngOnInit(): void {
    new ServiceUrlPreProcessor(this._context).parse(this.form);
    new TranslatePreProcessor(this._translateService).parse(this.form);
    this.submission = {
      data: this.filter
    };
    this.refreshForm.emit({
      form: this.form
    });
    this.loading = false;
    this.filterSubscription = this.filterChange.subscribe( filter => {
      this.submission.data = filter;
      this.refreshForm.emit({
        submission: this.submission
      });
    });
  }

  onChange(event: object) {
    Object.assign(this.filter, this.formComponent.submission.data);
  }

  onFormLoad(form: FormioForm) {
    //
  }

  ngAfterViewInit(): void {
    //
  }

  ngOnDestroy(): void {
    if (this.filterSubscription) {
      this.filterSubscription.unsubscribe();
    }
  }
}
