import {Component, ComponentDecorator, Directive, ElementRef, Input, OnDestroy, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {RouterModalOkCancel} from '@universis/common/routing';
import {ActivatedRoute, Router} from '@angular/router';
import {TableConfiguration} from '../advanced-table/advanced-table.interfaces';
import {Subscription} from 'rxjs';
import {AdvancedTableComponent, AdvancedTableDataResult} from '../advanced-table/advanced-table.component';

interface PageInfo {
  page: number;
  pages: number;
  start: number;
  end: number;
  length: number;
  recordsTotal: number;
  recordsDisplay: number;
  serverSide?: boolean;
}

// important: export this const due to AOT compilation errors
// see more https://angular.io/guide/aot-metadata-errors#only-initialized-variables
export const AdvancedTableModalBaseTemplate = `
<div>
    <!-- search -->
    <div class="row mt-3">
      <div class="col-3">
        <div class="input-group mb-3">
          <input spellcheck="false" type="text"
                 (keyup)="onKeyUp($event)" [(ngModel)]="searchText"
                 class="form-control bg-gray-100 border-0"
                 placeholder="{{'Tables.SearchListPlaceholder' | translate}}" aria-label="Search" aria-describedby="basic-addon2">
          <div class="input-group-append">
            <button class="btn btn-default bg-gray-100 border-0" type="button">
              <i class="fa fa-search text-secondary"></i>
            </button>
          </div>
        </div>
      </div>
      <div #emptyTable class="d-none">
        <div>
          <div class="mt-4">
            <div class="icon-circle bg-gray-300 border-gray-100"></div>
          </div>
          <div class="mt-4">
            <h4 class="font-3xl text-dark" [translate]="'Tables.EmptyTableTitle'"></h4>
            <p class="font-lg text-dark" [translate]="'Tables.EmptyTableMessage'"></p>
          </div>

        </div>
      </div>
      <div class="col-3">
        <div class="btn-toolbar">
          <button class="btn btn-gray-100">
            <i class="fa fa-sliders-h pr-2 text-secondary" aria-hidden="true"></i>{{'Tables.Filters' | translate}}
          </button>
          <div class="dropdown ml-3">
            <button class="btn btn-gray-100 dropdown-toggle"
                    type="button" id="dropdownDefaultFilters"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              {{ this.defaultFilter.name | translate }}
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownDefaultFilters">
              <a class="dropdown-item" *ngFor="let filter of defaultFilters" href="#">{{ filter.name | translate }}</a>
            </div>
          </div>
        </div>
      </div>
      <div class="col">
        <button [disabled]="true" class="float-right btn rounded-pill btn-outline-theme">
          <span class="pr-2 font-lg">+</span><span [translate]="'Tables.AddItem'"></span>
        </button>
      </div>
    </div>
    <div class="row">
      <div class="col-12" [ngClass]="{ 'component-loading': loading}">
        <app-advanced-table #advancedTable (init)="onTableInit($event)"
                            (load)="onDataLoad($event)"
                            [selectable]="true"
                            [multipleSelect]="true"
                            [scrollX]="false" [scrollY]="320"
                            [pageLength]="pageLength"
                            [scrollable]="false" [showFooter]="false"
                            [showActions]="false" [lengthMenu]="lengthMenu"
                            [config]="tableConfig"></app-advanced-table>
      </div>
    </div>
    <div class="row mt-3 table-custom-pager">
      <div class="col-12 d-flex" *ngIf="recordsTotal">
        <div class="mt-2 ml-auto mr-3 text-secondary" [translate]="'Tables.RecordsPerPage'"></div>
        <div class="dropdown mr-3">
          <button class="btn btn-gray-100 dropdown-toggle"
                  type="button" id="dropdownPageSize" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            {{pageLength}}
          </button>
          <div class="dropdown-menu" aria-labelledby="dropdownPageSize">
            <button (click)="setLength(item)"
                    class="dropdown-item dropdown-item-theme"
                    type="button" *ngFor="let item of lengthMenu">{{item}}</button>
          </div>
        </div>
        <div class="mt-2 mr-2" [translate]="'Tables.PageInfo'" [translateParams]="pageInfo"></div>
        <button [disabled]="pageInfo && pageInfo.page === 0" class="btn text-nowrap" (click)="firstPage()">
          <i class="fa fa-chevron-left text-color-theme"></i>
          <i class="fa fa-chevron-left text-color-theme"></i>
        </button>
        <button [disabled]="pageInfo && pageInfo.page===0" class="btn" (click)="previousPage()">
          <i class="fa fa-chevron-left text-color-theme"></i>
        </button>
        <button [disabled]="pageInfo && pageInfo.page===pageInfo.pages-1" class="btn" (click)="nextPage()">
          <i class="fa fa-chevron-right text-color-theme"></i>
        </button>
        <button [disabled]="pageInfo && pageInfo.page===pageInfo.pages-1" class="btn text-nowrap" (click)="lastPage()">
          <i class="fa fa-chevron-right text-color-theme"></i>
          <i class="fa fa-chevron-right text-color-theme"></i>
        </button>
      </div>
    </div>
  </div>
`;


@Component({
  selector: 'app-advanced-table-modal',
  template: AdvancedTableModalBaseTemplate
})
export class AdvancedTableModalBaseComponent extends RouterModalOkCancel implements OnInit, OnDestroy {

  private static readonly DEFAULT_ALL_FILTER = {
    name: 'Tables.All',
    filter: null
  };

  @ViewChild('advancedTable') advancedTable: AdvancedTableComponent;
  @ViewChild('emptyTable') emptyTable: ElementRef;
  @Input('tableConfig') tableConfig: TableConfiguration;
  @Input('pageLength') pageLength = 10;
  @Input('searchText') searchText: string;
  @Input('lengthMenu') lengthMenu: Array<number> = [5, 10, 25, 50, 100];
  private subscription: Subscription;
  public recordsTotal = 0;
  public loading = false;
  public recordsFiltered = 0;
  public defaultFilters: Array<any> = [];
  @Input('defaultFilter') defaultFilter = AdvancedTableModalBaseComponent.DEFAULT_ALL_FILTER;
  public pageInfo = <PageInfo> {
    serverSide: true
  };

  constructor(_router: Router, _activatedRoute: ActivatedRoute) {
    super(_router, _activatedRoute);
    this.modalClass = 'modal-xl modal-table';
  }

  private _getInputs(componentType): Array<any> {
    const props = componentType.__prop__metadata__;
    const inputs = [];
    if (props == null) {
      return inputs;
    }
    Object.keys(props).forEach (prop => {
      const member = props[prop][0];
      if (member.ngMetadataName === 'Input') {
        inputs.push(prop);
      }
    });
    return inputs.sort();
  }

  public hasInputs(): Array<string> {
    return [];
  }

  ngOnInit() {

    this.okButtonText = 'Tables.Apply';
    this.cancelButtonText = 'Tables.Cancel';

    // get route data
    this.subscription = this.activatedRoute.data.subscribe( data => {
      // set modal title if any
      if (data.title) {
        this.modalTitle = data.title;
      }
      if (data.config) {
        this.tableConfig = data.config;
      }
      // important: use hasInputs due to AOT compilation. Metadata properties cannot be used (e.g. @Input())
      // because there are empty during runtime
      // so we need to define manually input properties that are going to be bind through this process
      // bind all inputs
      const inputs = this.hasInputs();
      inputs.forEach( input => {
        if (Object.prototype.hasOwnProperty.call(data, input)) {
          this[input] = data[input];
        }
      });
    });
  }

  cancel(): Promise<any> {
    return this.close();
  }

  ok(): Promise<any> {
    return this.close();
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
    this.recordsFiltered = data.recordsFiltered;
    // get first page info (because page is not going to be fired for first page)
    if (data.recordsTotal > 0 && this.pageInfo.recordsTotal === 0) {
      this.pageInfo.recordsTotal = data.recordsTotal;
    }
    const self = this;
  }

  previousPage() {
    // go to next page
    // noinspection TypeScriptValidateJSTypes
    this.advancedTable.dataTable.page('previous').draw('page');
  }

  nextPage() {
    // go to next page
    // noinspection TypeScriptValidateJSTypes
    this.advancedTable.dataTable.page('next').draw('page');
  }

  onTableInit(dataTable: any) {
    const self = this;
    // get initial page info
    const initialPageInfo = <PageInfo>dataTable.page.info();
    initialPageInfo.start += 1;
    self.pageInfo = initialPageInfo;
    // change emptyTableMessage
    dataTable.context[0].oLanguage.sEmptyTable = this.emptyTable.nativeElement.innerHTML;
    // bind page change event
    dataTable.on( 'page.dt', function() {
      const pageInfo = <PageInfo>dataTable.page.info();
      pageInfo.start += 1;
      self.pageInfo = pageInfo;
    });
    // bind draw change event
    dataTable.on( 'draw.dt', function() {
      // for future use
      self.loading = false;
      const pageInfo = <PageInfo>dataTable.page.info();
      pageInfo.start += 1;
      self.pageInfo = pageInfo;
    });
  }

  firstPage() {
    // go to first page
    // noinspection TypeScriptValidateJSTypes
    this.advancedTable.dataTable.page('first').draw('page');
  }

  lastPage() {
    // go to last page
    // noinspection TypeScriptValidateJSTypes
    this.advancedTable.dataTable.page('last').draw('page');
  }

    setLength(value: number) {
      this.pageLength = value;
      this.advancedTable.dataTable.page.len(value).draw();
    }

  onKeyUp(event: KeyboardEvent) {
    if (event.code === 'Enter') {
      // loading
      this.loading = true;
      this.advancedTable.dataTable.search((<HTMLInputElement>event.target).value).draw();
    }
  }
}
