import { Injectable } from '@angular/core';
import { template, at } from 'lodash';
@Injectable({
  providedIn: 'root'
})
export class AdvancedFilterValueProvider {

  public values = { };

  constructor() {
    //
  }

  buildFilter(filter) {
    if (filter) {
      return template(filter)(this.values);
    }
  }

  async asyncBuildFilter(filter) {
    if (filter) {
      return template(filter)(await this.getValues());
    }
  }

  async getValues() {
    return this.values;
  }

}
