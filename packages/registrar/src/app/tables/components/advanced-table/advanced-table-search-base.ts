import {EventEmitter, Input, Output} from '@angular/core';

export class AdvancedTableSearchBaseComponent {
    private _filter: any;
    @Output() filterChange = new EventEmitter();

    @Input()
    get  filter() {
        return this._filter;
    }

    set filter(value) {
        this._filter = value;
        this.filterChange.emit(value);
    }

    onKeyEnter(event: KeyboardEvent) {
        if (event.code === 'Enter') {
            this.filterChange.emit(this._filter);
        }
    }
}
