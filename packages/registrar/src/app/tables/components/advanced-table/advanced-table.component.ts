import {
  ChangeDetectorRef,
  Component,
  ElementRef, EventEmitter,
  Inject,
  InjectionToken,
  Injector,
  Input, OnDestroy,
  OnInit, Output,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import 'datatables';
import {TranslatePipe, TranslateService} from '@ngx-translate/core';
import {template} from 'lodash';
import {ActivatedRoute} from '@angular/router';
import {TableConfiguration} from './advanced-table.interfaces';
import {DataServiceQueryParams} from '@themost/client/src/common';
import {AdvancedColumnFormatter} from './advanced-table.formatters';
import {ClientDataQueryable, TextUtils} from '@themost/client';
import {AdvancedFilterValueProvider} from './advanced-filter-value-provider.service';
import {Subscription} from 'rxjs';

declare var $: any;

export interface AdvancedTableDataResult {
  recordsTotal: number;
  recordsFiltered: number;
  data: Array<any>;
}

class TableTranslationChangeDetector extends ChangeDetectorRef {
  checkNoChanges(): void {
  }

  detach(): void {
  }

  detectChanges(): void {
  }

  markForCheck(): void {
  }

  reattach(): void {
  }

}

export let COLUMN_FORMATTERS = new InjectionToken('column.formatters');


class ColumnFormatter extends AdvancedColumnFormatter {
  constructor(protected injector: Injector) {
    super();
  }
  public class: string;
  public className: string;
  public defaultContent: string;
  public formatString: string;
  public formatter: string;
  public hidden: boolean;
  public name: string;
  public order: string;
  public property: string;
  public sortable: boolean;
  public title: string;
  render(data: any, type: any, row: any, meta: any) {
    //
  }
}

function forceCast<T>(input: any): T {
  return input;
}

export class AdvancedTableConfiguration {
  static cast(input: any): TableConfiguration {
    return forceCast<TableConfiguration>(input);
  }
}

function getRandomIdentifier() {
  return (
      Number(String(Math.random()).slice(2))
  ).toString(36);
}

@Component({
  selector: 'app-advanced-table',
  template: `
    <table #table class="dataTable">
    </table>
    <div #emptyTable class="d-none">
      <div>
        <div class="mt-4">
          <div class="icon-circle bg-gray-300 border-gray-100"></div>
        </div>
        <div class="mt-4">
          <h4 class="font-3xl text-dark" [translate]="'Tables.EmptyTableTitle'"></h4>
          <p class="font-lg text-dark" [translate]="'Tables.EmptyTableMessage'"></p>
        </div>
      </div>
    </div>
  `,
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./advanced-table.component.scss']
})
export class AdvancedTableComponent implements OnInit, OnDestroy {


  private _query: ClientDataQueryable;
  private _config: TableConfiguration;
  private _firstLoad = true;
  private _showFooter = true;
  private _showPaging = false;
  private _id = getRandomIdentifier();


  @ViewChild('table') table: ElementRef;
  @ViewChild('emptyTable') emptyTable: ElementRef;
  @Input() height = 800;
  @Input() showActions = true;
  @Input() autoLoad = true;
  @Output('load') load: EventEmitter<AdvancedTableDataResult> = new EventEmitter<AdvancedTableDataResult>();
  @Output('init') init: EventEmitter<any> = new EventEmitter<any>();
  @Input('reload') reload: EventEmitter<any> = new EventEmitter<any>();

  @Input() scrollable = true;
  @Input() serverSide = true;
  @Input() lengthMenu = [50, 100, 200, 500];
  @Input() pageLength = 25;

  @Input() scrollY = 800;
  @Input() scrollX = false;
  @Input() selectable = false;
  @Input() multipleSelect = false;
  @Input() selected = [];

  @Input('config')
  public set config(value: TableConfiguration) {
    this._config = value;
  }

  public get config() {
    return this._config;
  }

  @Input('showFooter')
  public set showFooter(value: boolean) {
    this._showFooter = value;
    // show or hide footer
    if (this._element && this._element.nativeElement) {
      const element = (<HTMLDivElement>this._element.nativeElement)
          .querySelector('.dataTables_info');
      if (element == null) {
        return;
      }
      if (value) {
        element.classList.remove('d-none');
      } else {
        element.classList.add('d-none');
      }
    }
  }

  public get showFooter() {
    return this._showFooter;
  }

  @Input('showPaging')
  public set showPaging(value: boolean) {
    this._showPaging = value;
    // show or hide footer
    if (this._element && this._element.nativeElement) {
      const element = (<HTMLDivElement>this._element.nativeElement)
          .querySelector('.dataTables_paginate');
      if (element == null) {
        return;
      }
      if (value) {
        $(element).show();
      } else {
        $(element).hide();
      }
    }
  }

  public get showPaging() {
    return this._showPaging;
  }

  @Input('query')
  public set query(value: ClientDataQueryable) {
    this._query = value;
  }

  public get query() {
      return this._query;
  }

  public readonly translator: TranslatePipe;
  public dataTable: any;
  dtOptions: any;
  private lastQueryParams: DataServiceQueryParams;
  private reloadSubscription: Subscription;

  constructor(private _context: AngularDataContext,
              @Inject(COLUMN_FORMATTERS) private _columnFormatters: Array<Function>,
              private _activatedRoute: ActivatedRoute,
              private _translateService: TranslateService,
              private _injector: Injector,
              private _advancedFilterValueProvider: AdvancedFilterValueProvider,
              private _element: ElementRef) {
    // initialize translate pipe
    this.translator = new TranslatePipe(this._translateService, new TableTranslationChangeDetector());
    $.fn.dataTable.ext.classes.sPageButton = 'btn';
    $.fn.dataTable.ext.classes.sPageButtonActive = 'btn btn-light';
    this.reloadSubscription = this.reload.subscribe( data => {
      this.fetch();
    });
  }

  search(text) {
    this.dataTable.search(text).draw();
  }

  fetch(clearSelected?: boolean) {
    // if data table is null
    if (this.dataTable == null) {
      // initialize data table
      return this.ngOnInit();
    }
    if (clearSelected) {
      this.selected.splice(0, this.selected.length);
    }
    // otherwise load data only
    this.dataTable.draw();
  }

  destroy() {
    if (this.dataTable) {
      this.dataTable.destroy();
    }
  }

  // noinspection JSMethodCanBeStatic
  /**
   * Datatables column renderer for select checkbox
   * @param data
   * @param type
   * @param row
   * @param meta
   * @private
   */
  private _renderSelect(data, type, row, meta) {
    // get column
    const column = meta.settings.aoColumns[meta.col];
    // render template
    return template(`<input class="checkbox checkbox-theme s-row" type="checkbox"
 id="s-${this._id}-${meta.row}" data-id="${data}" /><label for="s-${this._id}-${meta.row}">&nbsp;</label>`)(row);
  }

  ngOnInit() {
    const self = this;
    if (this.dataTable) {
      this.dataTable.destroy();
      $(this.table.nativeElement).empty();
    }
    if (this.config) {
      // prepare configuration for columns
      const tableColumns = this.config.columns
          // filter action link column
          .filter( column => {
        if (column.formatter && column.formatter === 'ActionLinkFormatter' && this.showActions === false) {
          return false;
        }
        return true;
      }).map( column => {
        // convert column to advance table column
        return Object.assign(new ColumnFormatter(this._injector), column);
      }).map( column => {
        const columnDefinition = {
          data: column.property || column.name,
          defaultContent: column.hasOwnProperty('defaultContent') ? column.defaultContent : '',
          title: this.translator.transform(column.title),
          sortable: column.hasOwnProperty('sortable') ? column.sortable : true,
          visible: column.hasOwnProperty('hidden') ? !column.hidden : true,
          name: column.name
        };

        if (column.className) {
          Object.assign(columnDefinition, {
            className: column.className
          });
        } else {
          Object.assign(columnDefinition, {
            className: ''
          });
        }

        if (column.formatters) {
          Object.assign(columnDefinition, {
            render: function(data, type, row, meta) {
              let result = data;
              column.formatters.forEach( columnFormatter => {
                const formatter = self._columnFormatters.find(x => {
                  return x.name === columnFormatter.formatter;
                });
                if (formatter && typeof formatter.prototype.render === 'function') {
                  column.formatString = columnFormatter.formatString;
                  column.formatOptions = columnFormatter.formatOptions;
                }
                result = formatter.prototype.render.bind(column)(result, type, row, meta);
              });
              delete column.formatString;
              delete column.formatOptions;
              return result;
            }
          });
        }
        if (column.formatter) {
          const formatter = this._columnFormatters.find( x => {
            return x.name === column.formatter;
          });
          if (formatter && typeof formatter.prototype.render === 'function') {
            Object.assign(columnDefinition, {
              render: formatter.prototype.render.bind(column)
            });
          }
        }
        return columnDefinition;
      });
      // get table element
      const tableElement = $(this.table.nativeElement);
      // allow selection
      if (this.selectable) {
        // get first column (as row primary key)
        // todo::allow row primary key configuration
        const firstColumn = this.config.columns[0];
        // add select column
        const selectColumnDefinition = {
          className: 'text-center pl-4 pr-0',
          data: firstColumn.property || firstColumn.name,
          defaultContent: '',
          title: '',
          sortable: false,
          visible: true,
          name: firstColumn.name,
        };
        // assign renderer
        Object.assign(selectColumnDefinition, {
          render: this._renderSelect.bind(this)
        });
        // and finally add column
        tableColumns.unshift(selectColumnDefinition);
        // user interface additions
        // handle page change event (a trick for firing draw.dt events)
        tableElement
            // for each page
            .on('page.dt', ($event) => {
              const info = this.dataTable.page.info();
            })
            // handle draw events
            .on('draw.dt', () => {
              tableElement.find('input.s-row').on('click', this.onSelectionChange.bind(this));
              // make selection on current page
              if (this.selected.length > 0) {
                // enumerate selected items
                this.selected.forEach( item => {
                  // get row primary key
                  const key = this.config.columns[0].property;
                  // find row identifier
                  const id = item[key];
                  // if identifier is defined
                  if (id != null) {
                    // try to find check element with this identifier in attribute data-id
                    tableElement.find(`input[data-id='${id}']`).prop('checked', true);
                  }
                });
              }
            });
      }
      // initialize data table
      const settings = {
        // set length menu
        lengthMenu: [5, 10, 50, 100, 200, 500],
        // hide length menu
        lengthChange: false,
        // enable search but hide search box (css)
        searching: true,
        // enable data processing
        processing: true,
        // enable getting server side data
        serverSide: this.serverSide,
        // enumerate table columns
        columns: tableColumns,
        order: [],
        // set scroll y
        scrollY: this.scrollY,
        // set scroll x
        scrollX: this.scrollX,
        dom: 'Bfrtip',
        buttons: [
            {
              extend: 'excel',
              filename: this.config.title,
              className: 'd-none',
              exportOptions: {
                columns: 'th:not(:first-child)'
              }
           }
         ],
        // define server data callback
        fnServerData: function (sSource, aoData, fnCallback, oSettings) {
          // if method is invoked for the first time
          if (self._firstLoad) {
            // disabled first load flag
            self._firstLoad = false;
            // if auto load is disabled
            if (self.autoLoad === false) {
              const emptyDataSet = {
                recordsTotal: 0,
                recordsFiltered: 0,
                data: []
              };
              self.load.emit(emptyDataSet);
              // return empty data
              return fnCallback(emptyDataSet);
            }
          }
          // get activated route params
          const queryParams = self._activatedRoute.snapshot.queryParams;
            // get columns
            const columns = aoData[1].value;
            // get order expression
            const order = aoData[2].value;
            // get skip records param
            const skip = aoData[3].value;
            // get page size param
            const top = aoData[4].value;
            // get search value
            const searchText = aoData[5].value;
            // get data queryable
            const q = self._query instanceof ClientDataQueryable ? self._query : self._context.model(self.config.model).asQueryable();
            // apply paging params
            if (top) {
              q.take(top).skip(skip);
            }
            // apply order params
            if (order && order.length) {
              // get order query expression
              const orderByStr = order.map( expr => {
                return self.config.columns[expr.column].name + ' ' + expr.dir || 'asc';
              }).join(',');
              // set order
              q.setParam('$orderby', orderByStr);
            }
            // configure $select parameter
            const select = self.config.columns.map( column => {
              if (column.property) {
                return column.name + ' as ' + column.property;
              } else {
                return column.name;
              }
            });
            q.select.apply(q, select);
            // configure $filter
            if (queryParams.$filter && queryParams.$filter.length) {
              // set route filter
              q.setParam('$filter', queryParams.$filter).prepare();
            }
            // check if query parameters contain filter
            if (queryParams.$expand && queryParams.$expand.length) {
              // set route $expand
              q.setParam('$expand', queryParams.$expand).prepare();
            }
            if (searchText && searchText.value && self.config.searchExpression) {
              // validate search text in double quotes
              if (/^"(.*?)"$/.test(searchText.value)) {
                q.setParam('$filter',
                  template(self.config.searchExpression)({
                    text: searchText.value.replace(/^"|"$/g, '')
                  }));
              } else {
                // try to split words
                const words = searchText.value.split(' ');
                // join words to a single filter
                const filter = words.map( word => {
                  return template(self.config.searchExpression)({
                    text: word
                  });
                }).join (' and ');
                // set filter
                q.setParam('$filter', filter);
              }
            } else {
              // clear filter
              q.setParam('$filter', null);
            }

            if (!q.getParams()['$orderby'] && self.config && self.config.defaults && self.config.defaults.orderBy) {
              q.setParam('$orderby', self.config.defaults.orderBy);
            }

            // append default filter
            if (self.config && self.config.defaults && self.config.defaults.filter) {
              return self._advancedFilterValueProvider.asyncBuildFilter(self.config.defaults.filter).then( defaultFilterString => {
                if (defaultFilterString) {
                  q.prepare().filter(defaultFilterString);
                }
                self.lastQueryParams = q.getParams();
                q.getList().then( items => {
                  const dataSet = {
                    recordsTotal: items.total,
                    recordsFiltered: items.total,
                    data: items.value
                  };
                  // emit load event
                  self.load.emit(dataSet);
                  // return data
                  return fnCallback(dataSet);
                });
              });
            }
            self.lastQueryParams = q.getParams();
            // hide the dataTable and show the processing... label
            $(self._element.nativeElement).find('.dataTables_scrollBody>.dataTable.no-footer').hide();
            $(self._element.nativeElement).find('.dataTables_processing').show();
            q.getList().then( items => {
              // hide tha processing... label and show the dataTable
              $(self._element.nativeElement).find('.dataTables_scrollBody>.dataTable.no-footer').show();
              $(self._element.nativeElement).find('.dataTables_processing').hide();
              const dataSet = {
                recordsTotal: items.total,
                recordsFiltered: items.total,
                data: items.value
              };
              // emit load event
              self.load.emit(dataSet);
              // return data
              return fnCallback(dataSet);
            });
        },
        language: self.translator.transform('Tables.DataTable')
      };

      if (this.scrollable) {
        // set continuous scrolling
        Object.assign(settings, {
          scrollCollapse: false,
          scroller: {
            loadingIndicator: false,
            displayBuffer: 10
          },
        });
      } else {
        Object.assign(settings, {
          paging: true,
          pageLength: this.pageLength
        });
      }
      if (this.selectable) {

      }
      // handle init.dt event and emit local event
      this.dataTable = tableElement.on('init.dt', () => {
        this.dataTable.context[0].oLanguage.sEmptyTable = this.emptyTable.nativeElement.innerHTML;
        this.init.emit(this.dataTable);
      }).DataTable(settings);
      // show or hide footer
      if (!this.showFooter) {
        (<HTMLDivElement>this._element.nativeElement)
            .querySelector('.dataTables_info').classList.add('d-none');
      }
      // show or hide pager
      if (this.showPaging) {
        this.showPaging = true;
      }
    }
  }

  ngOnDestroy(): void {
    if (this.reloadSubscription) {
      this.reloadSubscription.unsubscribe();
    }
  }

  /**
   * Handle selection change
   * @param $event
   */
  onSelectionChange($event) {
    const input = <HTMLInputElement>$event.target;
    // get row from id
    const rowIndex = parseInt(/-(\d+)$/g.exec(input.id)[1], 10);
    // get row
    const row  = this.dataTable.row(rowIndex).data();
    // get row identifier
    if (row) {
      const key = this.config.columns[0].property;
      const id = row[key];
      const findItemIndex = this.selected.findIndex( item => {
        return item[key] === id;
      });
      if (input.checked) {
        if (findItemIndex < 0) {
          // add row to selected rows
          this.selected.push(row);
        }
      } else {
        if (findItemIndex >= 0) {
          // remove selected item
          this.selected.splice(findItemIndex, 1);
        }
      }
    }
  }

  export () {
    const element: HTMLElement = document.querySelector('.dt-buttons .buttons-excel') as HTMLElement;
    element.click();
  }



  handleColumns (columnsToShow) {
    const table = this.dataTable;
    this.config.columns.slice(1).forEach((column, i) => {
      if (!column.hidden) {
        if (columnsToShow.includes(column.name)) {
          // noinspection TypeScriptValidateJSTypes
          table.column(column.name + ':name').visible(true);
        } else {
          // noinspection TypeScriptValidateJSTypes
          table.column(column.name + ':name').visible(false);
        }
      }
    });
  }

  getConfig () {
    return this.config;
  }

}
