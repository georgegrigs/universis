import {Directive, HostListener} from '@angular/core';
import {ActivatedTableService} from '../tables.activated-table.service';

@Directive({
    selector: '.universis-btn-export'
})
export class BtnExpDirective {
  constructor(private _activatedTable: ActivatedTableService) {}

  @HostListener('click', ['$event'])
  clickEvent(event) {
    event.preventDefault();
    event.stopPropagation();
    if (this._activatedTable.activeTable) {
      this._activatedTable.activeTable.export();
    }
  }

}
