import {InjectionToken, NgModule, OnInit} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AdvancedTableComponent, COLUMN_FORMATTERS} from './components/advanced-table/advanced-table.component';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {environment} from '../../environments/environment';
import {AdvancedTableSearchComponent} from './components/advanced-table/advanced-table-search.component';
import { DatePipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {
    ActionLinkFormatter,
    LinkFormatter,
    NestedPropertyFormatter,
    TemplateFormatter,
    TrueFalseFormatter,
    TranslationFormatter,
    DateTimeFormatter,
    NgClassFormatter,
    LanguageFormatter, SelectRowFormatter
} from './components/advanced-table/advanced-table.formatters';
import {ActivatedTableService} from './tables.activated-table.service';
import {BtnExpDirective} from './directives/btnexp.directive';
import {AdvancedTableSettingsComponent} from './components/advanced-table/advanced-table-settings.component';
import {SharedModule} from '@universis/common';
import {AdvancedFilterValueProvider} from './components/advanced-table/advanced-filter-value-provider.service';
import { AdvancedTableModalBaseComponent } from './components/advanced-table-modal/advanced-table-modal-base.component';
import {RouterModalModule} from '@universis/common/routing';
import { AdvancedSearchFormComponent } from './components/advanced-search-form/advanced-search-form.component';
import {AdvancedFormsModule} from '@universis/forms';
import {FormioModule} from 'angular-formio';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        TranslateModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModalModule,
        FormioModule,
        AdvancedFormsModule
    ],
    providers: [DatePipe,
      {
        provide: COLUMN_FORMATTERS, useValue: [
          NestedPropertyFormatter,
          LinkFormatter,
          TemplateFormatter,
          TrueFalseFormatter,
          ActionLinkFormatter,
          TranslationFormatter,
          DateTimeFormatter,
          NgClassFormatter,
          ActivatedTableService,
          AdvancedFilterValueProvider,
          LanguageFormatter,
          SelectRowFormatter
        ]
      }
      ],
    declarations: [
        AdvancedTableComponent,
        AdvancedTableSearchComponent,
        BtnExpDirective,
        AdvancedTableSettingsComponent,
        AdvancedTableModalBaseComponent,
        AdvancedSearchFormComponent
    ],
    exports: [
        AdvancedTableComponent,
        AdvancedTableSearchComponent,
        BtnExpDirective,
        AdvancedTableSettingsComponent,
        AdvancedSearchFormComponent
    ],
    entryComponents: [ AdvancedTableSettingsComponent ]
})
export class TablesModule implements OnInit {
    constructor(private _translateService: TranslateService) {
        this.ngOnInit().catch(err => {
            console.error('An error occurred while loading tables module');
            console.error(err);
        });
    }

    async ngOnInit() {
        environment.languages.forEach(language => {
            import(`./i18n/tables.${language}.json`).then((translations) => {
                this._translateService.setTranslation(language, translations, true);
            });
        });
    }

}
