import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import * as THESES_LIST_CONFIG from './theses-table.config.json';
import {AdvancedTableComponent} from '../../../tables/components/advanced-table/advanced-table.component';
import {ActivatedRoute, Router} from '@angular/router';


@Component({
  selector: 'app-theses-table',
  templateUrl: './theses-table.component.html'
})
export class ThesesTableComponent implements OnInit {

  public readonly config = THESES_LIST_CONFIG;

  @ViewChild('table') table: AdvancedTableComponent;


  constructor(private _router: Router, private _activatedRoute: ActivatedRoute) { }

  ngOnInit() {

  }

  onSearchKeyDown(event: any) {
    if (event.keyCode === 13) {
      this.table.search((<HTMLInputElement>event.target).value);
      return false;
    }
  }




}
