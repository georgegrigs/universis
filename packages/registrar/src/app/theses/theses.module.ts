import {CUSTOM_ELEMENTS_SCHEMA, NgModule, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThesesHomeComponent } from './components/theses-home/theses-home.component';
import {ThesesSharedModule} from './theses.shared';
import {ThesesRoutingModule} from './theses.routing';
import {TablesModule} from '../tables/tables.module';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {environment} from '../../environments/environment';
import {ThesesTableComponent} from './components/theses-table/theses-table.component';
import { ThesesPreviewComponent } from './components/theses-preview/theses-preview.component';
import { ThesesRootComponent } from './components/theses-root/theses-root.component';
import { ThesesPreviewGeneralComponent } from './components/theses-preview-general/theses-preview-general.component';
import { ThesesPreviewStudentsComponent } from './components/theses-preview-general/theses-preview-students.component';
import {SharedModule} from '@universis/common';
import {FormsModule} from '@angular/forms';
import {ElementsModule} from '../elements/elements.module';
import {StudentsSharedModule} from '../students/students.shared';
import {ThesesAdvancedTableSearchComponent} from './components/theses-table/theses-advanced-table-search.component';
import {MostModule} from '@themost/angular';
import { AdvancedFormsModule } from '@universis/forms/lib/advanced-forms.module';
import {RegistrarSharedModule} from '../registrar-shared/registrar-shared.module';

@NgModule({
  imports: [
    CommonModule,
    ThesesSharedModule,
    ThesesRoutingModule,
    TranslateModule,
    TablesModule,
    FormsModule,
    ElementsModule,
    SharedModule,
    StudentsSharedModule,
    MostModule,
    AdvancedFormsModule,
    RegistrarSharedModule
  ],
  declarations: [ThesesHomeComponent,
    ThesesTableComponent,
    ThesesPreviewComponent,
    ThesesRootComponent,
    ThesesPreviewGeneralComponent,
    ThesesPreviewStudentsComponent,
    ThesesAdvancedTableSearchComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ThesesModule {

  constructor(private _translateService: TranslateService) {
    //
  }
}
