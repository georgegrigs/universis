import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ThesesHomeComponent} from './components/theses-home/theses-home.component';
import {ThesesTableComponent} from './components/theses-table/theses-table.component';
import {StudentsTableComponent} from '../students/components/students-table/students-table.component';
import {ThesesRootComponent} from './components/theses-root/theses-root.component';
import {ThesesPreviewComponent} from './components/theses-preview/theses-preview.component';
import {ThesesPreviewGeneralComponent} from './components/theses-preview-general/theses-preview-general.component';
import {ThesesPreviewStudentsComponent} from './components/theses-preview-general/theses-preview-students.component';
import {AdvancedFormRouterComponent} from '../registrar-shared/advanced-form-router/advanced-form-router.component';

const routes: Routes = [
    {
        path: '',
        component: ThesesHomeComponent,
        data: {
            title: 'Theses'
        },
      children: [
        {
          path: '',
          pathMatch: 'full',
          redirectTo: 'list'
        },
        {
          path: 'list',
          component: ThesesTableComponent,
          data: {
            title: 'StudentTheses List'
          }
        },
        {
          path: 'isPassed',
          component: ThesesTableComponent,
          data: {
            title: 'Completed Theses'
          }
        }
      ]
    },
    {
      path: ':id',
      component: ThesesRootComponent,
      data: {
        title: 'Theses Home'
      },
      children: [
        {
          path: '',
          pathMatch: 'full',
          redirectTo: 'preview'
        },
        {
          path: 'preview',
          component: ThesesPreviewComponent,
          data: {
            title: 'Theses Preview'
          },
          children: [
            {
              path: '',
              redirectTo: 'general'
            },
            {
              path: 'general',
              component: ThesesPreviewGeneralComponent,
              data: {
                title: 'Theses Preview General'
              }
            },
            {
              path: 'students',
              component: ThesesPreviewStudentsComponent,
              data: {
                title: 'Theses Students'
              }
            }
           ]
        },
        {
          path: ':action',
          component: AdvancedFormRouterComponent
        }
      ]
  }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: []
})
export class ThesesRoutingModule {
}
