import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-users-root',
  template: '<router-outlet></router-outlet>'
})
export class UsersRootComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
